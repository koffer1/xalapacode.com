+++
title = "Estamos en youtube"
date = "2019-06-27"
draft = false
thumbnail = ""
description = "XAlapacode esta en youtube"
author = "koffer"
author_uri = "/directorio/koffer"
markup = "mmark"
+++

![Estamos en youtube](/img/blog/youtube.jpg)


## Estamos en youtube

La comunidad de Xalapacode realiza cada mes una meetup donde se exponen diversos temas. Ahora podemos presumir que tenemos varias de estas platicas disponibles en video, las tenemos en el
[canal de la comunidad](https://www.youtube.com/channel/UCR7II0ABLovdY_vfBApiHCg/videos).

En este canal tenemos grabadas cuatro meetups. Las de [Marzo](https://www.youtube.com/watch?v=_6v3M-CIbaw&list=PLy5vbEYJmUWwl4rzi2V1hfEjSuXWZgARM), [Abril]
(https://www.youtube.com/watch?v=M4xoK26Sn8A&list=PLy5vbEYJmUWwIXzKrVdO_MlEdJv7l8YZQ), [Mayo](https://www.youtube.com/watch?v=Sys7J8JSfrw&list=PLy5vbEYJmUWy0HWerZNl4ugYoMl7Dvhj_) y el más reciente de [junio](https://www.youtube.com/watch?v=OwtJsc3fGMk&list=PLy5vbEYJmUWyS_x89Ei9q9GQDlCQwi71L).

Los invitamos a visitar estas platicas y si fueron expositores de alguna promover esa sus redes los videos donde salen.

También tenemos una [charla de Debian](https://www.youtube.com/watch?v=F2ryAaSkDF0&list=PLy5vbEYJmUWz-IrvAhJ6h6FuFxwrGau_5).

Recomiendanos y presume estas meetups!

