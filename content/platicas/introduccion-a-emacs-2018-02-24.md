+++
title = "Introducción a Emacs 2018-02-24"
date = "2018-02-25"
draft = false
description = "En esta plática se habló sobre los comandos básicos para utilizar emacs"
author = "Mauricio Téllez"
+++

En esta plática se habló sobre los comandos básicos para utilizar emacs así como consejos que pueden servir a los usuarios de otros editores de código.
