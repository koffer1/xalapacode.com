+++
title = "Meetup 2 de Marzo"
publishdate = "2018-02-25"
date = "2018-03-02"
event_place = "Facultad de Estadistica e Informatica, Av Xalapa S/N."
event_description = "Primer Meetup de Marzo"
event_flyer = "img/eventos/meetup-2018-03-02.jpg"
draft = true
+++

El 2 de Marzo se realiza el 2do Meetup del año y primero de Marzo. En esta ocasión todos los participantes son alumnos de la Facultad de Estadistica e Informatica de la Universidad Veracruzana. Las platicas son:
