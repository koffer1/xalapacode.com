+++
title = "Hacktoberfest"
publishdate = "2019-09-20"
date = "2019-10-12"
event_place = "Facultad de Estadística e Informática"
description = "El primer Hacktoberfest hospedado por la comunidad!"
thumbnail = "/img/eventos/shdh-2019-10-12-hacktoberfest.jpg"
author = "categulario"
draft = false
+++

[**Regístrate**](https://forms.gle/LxwXyeyHNWwV5QFdA)

## ¿Hacktoberquién?

El [hacktoberfest](https://hacktoberfest.digitalocean.com/) es un evento con duración de un mes organizado por [digitalocean](https://www.digitalocean.com/) que consiste en promover las contribuciones a proyectos de código abierto por parte de nosotros los desarrolladores.

## Dinámica

Para participar solo tienes que tener una cuenta de github y registrarte en [el sitio](https://hacktoberfest.digitalocean.com/). Si logras hacer cuatro pull-request a cualquier proyecto público hospedado en github a lo largo del mes (desde el 1 hasta el 31 de octubre) obtendrás una camisa de regalo.

## Premios

A los concursantes que hagan 4 pull-request se les enviará una camisa del evento además de stickers.

## ¿Y qué voy a ir a hacer a la FEI?

Nosotros nos reuniremos para apoyar a todos los interesados en participar en el proceso de contribución al open source, desde cómo usar git hasta cómo encontrar un proyecto relevante para contribuir y hacer tu primera contribución. [**Regístrate**](https://forms.gle/LxwXyeyHNWwV5QFdA)

## ¿Qué habrá en el evento?

Podrás encontrar las siguientes cosas:

* Mentores con muchas ganas de apoyarte,
* un grupo de amigos que les gusta echar el código,
* pizza, botanas y bebidas sin alcohol,
* una increíble oportunidad de volverte contribuidor de tu proyecto favorito

## Ok ya me convenciste, ¿dónde y a qué hora?

Nos reuniremos en el Laboratorio de electrónica (antes Laboratorio de Redes) de la Facultad de Informática de la Universidad Veracruzana el sábado 12 de octubre de 2019. El evento arranca a las 3:00 pm y se extiende hasta las 9 de la noche o hasta que nos echen a patadas del edificio. [**Regístrate**](https://forms.gle/LxwXyeyHNWwV5QFdA)

## ¿Qué necesito?

Tu propio equipo de cómputo, todo lo demás lo encontrarás en el evento. Además necesitas [registrarte aquí](https://forms.gle/LxwXyeyHNWwV5QFdA)

## Vale, me la rifo

Allá nos vemos!
